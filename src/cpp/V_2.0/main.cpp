#include <iostream>
#include <fstream>
#include <complex>
#include "WI.hpp"
#include <chrono>
#include <ctime>
#include <locale>

using namespace std::chrono;

int main(int argc, char *argv[])
  {
  std::string base_name, task_name;
  if(argc == 3){
    base_name = argv[1];
    task_name = argv[2]; //possible current tasks: spaghetti, berry_path, berry_mesh, gap_mesh
  }
  else{
    std::cout << "Error: remember to give as argument the case name and task to perform! %s\n" << argv[0];
    return 1;
  }

  auto t = std::time(nullptr);
  char mbstr[100];
  if (std::strftime(mbstr, sizeof(mbstr), "%A %c", std::localtime(&t))) 
      std::cout << mbstr << '\n';

  std::cout << "Task: " << task_name << std::endl << std::endl;
 
  TB::WI<std::complex<double> > H(base_name);
  high_resolution_clock::time_point t1 = high_resolution_clock::now();
  
  if(task_name=="velocity")
     H.vel_path(-0.05, 0.05);

  if(task_name=="spaghetti")
     H.spaghetti();

  if(task_name=="berry_path")
     H.berry_path();

  if(task_name=="berry_mesh")
     H.berry_mesh();

  if(task_name=="gap_mesh")
     H.gap_mesh();


  high_resolution_clock::time_point t2 = high_resolution_clock::now();
  auto duration = duration_cast<seconds>( t2 - t1 ).count();
  std::cout << "Task completed in: " << duration << " seconds" << std::endl;

  return 0;
  }
