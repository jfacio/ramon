#ifndef OP_def 
#define OP_def


#include<map>
#include<vector>
#include<iostream>
#include<sstream>
#include<string>
#include<sys/stat.h>
#include<boost/bimap.hpp>

//class for manipulating the matrix elements of an operator in real space

namespace OP_realspace{

  bool fileExists(const std::string& filename);
  template <class Type> class OP; 
  template<class Type> std::istream& operator>>(std::istream& is, OP<Type> & rhs);

  //typedef boost::bimap<int,std::tuple<int, int, int > > lattice_to_index:;

  template <class Type>
  class OP 
   {
   public:
     OP(){};
     int numwann(){ return (*this).num_wann;}
     int numwspoints(){ return (*this).nrpts;}
     friend std::istream& operator>><>(std::istream& is, OP<Type> & rhs); //to read wannier90_hr.dat file

   private:
     std::map <std::pair<int, int>, std::vector<Type> > O_mn; //map between indices n,m (corresponding to Wannier orbitals) and vector of matrix elements. Each j vector componenct correspond to the matrix element between the site 0 and site j and Wannier orbitals n and m.
     int nrpts; //number of Wigner-Seitz grid point
     int num_wann; //number of  Wannier orbitals in each site


   };

  template<class Type>
  std::istream& operator>>(std::istream& is, OP<Type> & rhs){
     std::cout << "Reading file: " <<std::endl;
     std::string line;
     
     int i =0;
     int deg_lines; //number of lines used for degeneracies of each point grid.
     while(std::getline(is,line)){
        if(i==1)
          rhs.num_wann = std::stoi(line);
        if(i==2){
          rhs.nrpts = std::stoi(line);
          deg_lines = rhs.nrpts / 15; //15 is a magic number defined by wannier90 code
          std::cout << "--Lines used for degeneracy information:  " << deg_lines << std::endl;
        }
        if(i>=deg_lines+3){
             std::istringstream iss(line);
              int ax,ay,az,m,n;
              double Omn_real,Omn_imag;
             if(iss >> ax >> ay >> az >> m >> n >> Omn_real >> Omn_imag) {
                std::cout << ax << ay << az << m << n << Omn_real << Omn_imag << std::endl;
                
             }
             else
               break; 

            
        }

        i++;
     }
      

  }

  bool fileExists(const std::string& filename){
    struct stat buf;
    if(stat(filename.c_str(),&buf)!=-1)
      return true;
    return false;
  }

}
#endif
