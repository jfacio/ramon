#!/usr/bin/python
import sys,os
sys.path.append(os.path.abspath("/home/jf11/repos/ramon/src/python/"))

import berry_methods


nx = 401
ny = 401
nz = 51

B = berry_methods.berry_dipole(nx,ny,nz,"kz")

#derivo OmegaX respecto de y
f_index = 0
coor_index = 1
delta_k = 0.0001
B.derive_by_plane(f_index,coor_index,delta_k)

#derivo OmegaX respecto de x
f_index = 0
coor_index = 0
B.derive_by_plane(f_index,coor_index,delta_k)

#derivo OmegaY respecto de y
f_index = 1
coor_index = 1
B.derive_by_plane(f_index,coor_index,delta_k)

#derivo OmegaX respecto de y
f_index = 1
coor_index = 1
B.derive_by_plane(f_index,coor_index,delta_k)
