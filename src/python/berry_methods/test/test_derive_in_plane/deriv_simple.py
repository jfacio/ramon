#!/usr/bin/python
import sys,os
sys.path.append(os.path.abspath("/home/jf11/repos/ramon/src/python/"))
import numpy as np
import berry_methods


#compute derivative of f(x,y) = cos(2*pi*x**2)*cos(2*pi*y**2)

b_1 = [1.0*0.01,0.0]
b_2 = [0.4*0.01,1.0*0.01]
p = []
f = []
for i in range(50):
  for j in range(50):
    x = i*b_1[0] + j*b_2[0]
    y = i*b_1[1] + j*b_2[1]
    p.append([x,y])
    f.append(np.cos(x*x*2*np.pi)*np.cos(y*y*2*np.pi))

extra_p = []
extra_f = []
for i in range(1,40):
  for j in range(-i,50+i):
    x = -i*b_1[0] + j * b_2[0]
    y = -i*b_1[1] + j * b_2[1]
    extra_p.append([x,y])
    extra_f.append(np.cos(x*x*2*np.pi)*np.cos(y*y*2*np.pi))
    x = (49+i)*b_1[0] + j * b_2[0]
    y = (49+i)*b_1[1] + j * b_2[1]
    extra_p.append([x,y])
    extra_f.append(np.cos(x*x*2*np.pi)*np.cos(y*y*2*np.pi))
    x = j*b_1[0] - i*b_2[0]
    y = j*b_1[1] - i*b_2[1]
    extra_p.append([x,y])
    extra_f.append(np.cos(x*x*2*np.pi)*np.cos(y*y*2*np.pi))
    x = j*b_1[0] + (49+i)*b_2[0]
    y = j*b_1[1] + (49+i)*b_2[1]
    extra_p.append([x,y])
    extra_f.append(np.cos(x*x*2*np.pi)*np.cos(y*y*2*np.pi))  



total_p = p
total_f = f
for i in range(len(extra_p)):
   total_p.append(extra_p[i])
   total_f.append(extra_f[i])

displacement = [0.00001,0]
der = berry_methods.derive_in_plane(displacement=displacement,points=total_p,function=total_f)
file = open('m2pixsin2pixxcos2piyy.dat','w')
for i in range(50*50):
  print >> file, p[i][0], p[i][1], der[i]
file.close()

displacement = [0,0.00001]
der = berry_methods.derive_in_plane(displacement=displacement,points=total_p,function=total_f)
file = open('m2piycos2pixxsin2piyy.dat','w')
for i in range(50*50):
  print >> file, p[i][0], p[i][1], der[i]
file.close()


file = open('extra_p.dat','w')
for i in range(len(extra_p)):
  print >> file, extra_p[i][0], extra_p[i][1], extra_f[i]
file.close()
