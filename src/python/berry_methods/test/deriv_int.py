#!/usr/bin/python
import sys,os
sys.path.append(os.path.abspath("/home/j/jf11/repos/ramon/src/python/"))

import berry_methods


nx = 401
ny = 401
nz = 51

B = berry_methods.berry_dipole(nx,ny,nz,"kz")

function = [[[None for k in xrange(nz)] for j in xrange(ny)] for i in xrange(nx)]
function2 = [[[None for k in xrange(nz)] for j in xrange(ny)] for i in xrange(nx)]

for i in range(nx):
  for j in range(ny):
     for k in range(nz):
        f = B.KZ[k]**2 * B.KX[i]
        function[i][j][k] = [B.KX[i],B.KY[j],B.KZ[k],f]
        f2 = B.KZ[k]**2 * B.KY[i]
        function2[i][j][k] = [B.KX[i],B.KY[j],B.KZ[k],f2]

B.Omega[0] = [function,'z**2x']
B.Omega[1] = [function2,'z**2y']

print 'Readed functions: ',B.Omega[0][1]
print 'Readed functions: ',B.Omega[1][1]

#B.interpolate(0.5,0.5,0,1)
#df_dx = B.deriv_function(0,0)
#D_f_x = B.integrate_function(df_dx)
#print 'D_x=',D_f_x

df_dy = B.deriv_function(1,1)
D_f_y = B.integrate_function(df_dy)
print 'D_y=',D_f_y

