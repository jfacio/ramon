#!/usr/bin/python
import sys,os
sys.path.append(os.path.abspath("/home/facioj/repos/ramon/src/python/"))
import numpy as np
import berry_methods


#compute the integral of the derivative with respect to y of f(x,y,z) = z**2y, and the integrate, considering that the fermi surface is the sphere of r < 0.1

create = eval(sys.argv[1])

b_1 = [1.0*0.1,0.0]
b_2 = [0.4*0.1,1.0*0.1]
function_file = 'f.dat'
energy_file = 'energy.dat'
coord_file = 'coord.dat'
delta_k = 0.0001
input_curve = "f.dat"

nx = 100
ny = 100
nz = 100
r = 0.005
energy_limit = 0
kz_names = []

for i in range(nz):
   name = "z_%s"%(i)
   kz_names.append(name)
   if(create):
       os.mkdir(name)
       os.chdir(name)
    
       z = 0.009*i/nz
       points = []
       function = []
       energy = []
    
       for i in range(nx):
         for j in range(ny):
           x = i*b_1[0]/nx + j*b_2[0]/ny
           y = i*b_1[1]/nx + j*b_2[1]/ny
           points.append([x,y,z])
           function.append( 1000*z**2 * y)
           if(x**2+y**2+z**2 < r**2):
             energy.append(-1)
           else:
             energy.append(1)
    
       file = open(function_file,'w')
       for f in function:
          print >> file, f
       file.close()
    
       file = open(energy_file,'w')
       for e in energy:
          print >> file, e
       file.close()
    
       file = open(coord_file,'w')
       for p in points:
          print >> file, p[0], p[1]
       file.close()
    
       os.chdir("..")

name0 = kz_names[0]
file = open("%s/%s.wpout"%(name0,name0),'w')
print >> file, "Units: ang "
print >> file, "b_1 ", b_1[0], b_1[1], 0.0
print >> file, "b_2 ", b_2[0], b_2[1], 0.0
print >> file, "b_3 ", 0.0, 0.0, z
file.close()

B = berry_methods.berry_dipole(nx=nx,ny=ny,nz=nz,root_kz="z")
B.bd_component(f_index=0,coor_index=1,delta_k=delta_k,kz_names=kz_names,energy_limit = energy_limit, input_curv = input_curve, input_coor = coord_file)

