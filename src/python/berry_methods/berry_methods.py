#!/usr/bin/python

import sys,os,re
import numpy as np
import math
from scipy.interpolate import griddata
import glob,shutil,subprocess


def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]

def derive_in_plane(**kwargs):
    """
    Necessary input:
    - displacement: [delta_x,delta_y] vector.
    - points: listt of domain points [ [kx1,ky1], [kx2,ky2] , ...]
    - function: values of f at the domaint Points [f1, f2, ...]

    Optional input:
    - energy: values of energy, to consider if sum the contribution or not.
    - energy_limit: energy limit to decide weather or not to include the k-point.
    - file_output: name for saving the auxiliary information. If not specified, it is not saved.

    If neither energy or energy_limit are provided, is like including all the points.

    return: list containing derivatives
    """

    displacement = kwargs['displacement']
    points = kwargs['points']
    function = kwargs['function']
    ene = kwargs.pop('energy', [-1 for i in range(len(function))])
    energy_limit = kwargs.pop('energy_limit',0)
    file_output = kwargs.pop('file_output', None)
      
    #build list of displaced points
    pxy_disp = []
    for point in points:
      pxy_disp.append([point[0]+displacement[0],point[1]+displacement[1]])

    #interpolate function to derive, as well as the energy, in the displaced points
    grid_inter_function = griddata(np.array(points), np.array(function), np.array(pxy_disp), method='cubic')
    grid_inter_ene = griddata(np.array(points), np.array(ene), np.array(pxy_disp), method='cubic')

    der = []
    to_output = []
    delta_k = np.sqrt(displacement[0]*displacement[0]+displacement[1]*displacement[1])
    for i in range(len(points)):
       if(grid_inter_ene[i] < energy_limit and ene[i] < energy_limit): 
         fp = (grid_inter_function[i]-function[i])/delta_k
         der.append(fp)
         to_output.append([points[i],fp,function[i],ene[i]])

    if(file_output != None):
      file_der = open(file_output,'w')
      for i in range(len(to_output)):
          print >> file_der, to_output[i][0][0],to_output[i][0][1], to_output[i][1], to_output[i][2], to_output[i][3]
      file_der.close()

    return der

def generate_kmesh(**kwargs):
    """
    Generates kz folders and as a result everything is ready to be computed.
    """
 
    nx = kwargs['nx']
    ny = kwargs['ny']
    nz = kwargs['nz']
    nz_i = kwargs.pop('nz_i',0)
    nz_f = kwargs.pop('nz_f',nz)
    kx = kwargs['kx_corner']
    ky = kwargs['ky_corner']
    file_win = kwargs['file_win']
    root_name = kwargs['root_name']
  
    base_name = os.path.basename(os.getcwd())

    file = open(file_win,'r')
    data_win = file.readlines()
    file.close()

    base_name = os.path.basename(os.getcwd())
    files_to_copy = ['*hr.dat', '*mmn*', '*amn*', '*win', '*eig*', '*chk']

    folder_name = """nx_%(nx)s_ny_%(ny)s_nz_%(nz)s"""%locals()
    os.mkdir(folder_name)
    for file_type in files_to_copy:
      for filename in glob.glob(os.path.join(".", file_type)):
         shutil.copy(filename,folder_name)
    os.chdir(folder_name)

    for iz in range(nz_i,nz_f + 1):
      #create directory
      name = "%(root_name)s_%(iz)s_%(nz)s"%locals()
      kz = iz*1.0/nz
      os.mkdir(name)

      for filename in glob.glob(os.path.join(".", '*win')):
        shutil.copy(filename,name)

      os.chdir(name)
      subprocess.call(['rename_files',base_name,name])
  
      #edit win file
      file = open('%s.win'%(name),'w')
      for fila in data_win:
        if 'kslice_corner' in fila:
          print >> file,' kslice_corner %s %s %s'%(kx,ky,kz)
        else:
          if 'kslice_2dkmesh' in fila:
             print >> file,' kslice_2dkmesh = %s %s '%(nx,ny)
          else: 
             print >> file,fila,
      file.close()

      os.chdir('..')


def split_berry_curvature(**kwargs):
      """
      file_name is the root name of the Wannier90 -kslice files.
      folder_name is the folder that hosts the Wannier90 -kslice files.
      kz is the value of kz to be print in the files.
      """

      file_name = kwargs['file_name']
      root_output = kwargs.pop('root_output','outsplit')
      folder = kwargs.pop('folder_name','./')
      kz = kwargs.pop('kz',0.0)

      print 'Reading ','''%(folder)s/%(folder)s-kslice-coord.dat'''%locals()
      file = open('''%(folder)s/%(folder)s-kslice-coord.dat'''%locals(),'r')
      data = file.readlines()
      file.close()

      X = []
      Y = []

      for fila in data[0:-1]:
         vals = map(eval,fila.split())
         X.append(vals[0])
         Y.append(vals[1])


      print 'Reading ','''%(folder)s/%(file_name)s'''%locals()
      file = open('''%(folder)s/%(file_name)s'''%locals(),'r')
      data = file.readlines()
      file.close()

      Fx = []
      Fy = []
      Fz = []

      for fila in data[0:-1]:
        vals = map(eval,fila.split())
        Fx.append(vals[0])
        Fy.append(vals[1])
        Fz.append(vals[2])

      file_x = open('''%(folder)s/%(root_output)s_x.dat'''%locals(),'w')
      file_y = open('''%(folder)s/%(root_output)s_y.dat'''%locals(),'w')
      file_z = open('''%(folder)s/%(root_output)s_z.dat'''%locals(),'w')

      y_old = Y[0]
      for i in range(len(X)-1):
        if((Y[i]-y_old)**2 > 1e-9):
           print >> file_x, '\n', X[i],Y[i],kz,Fx[i]
           print >> file_y, '\n', X[i],Y[i],kz,Fy[i]
           print >> file_z, '\n', X[i],Y[i],kz,Fz[i]
        else:
           print >> file_x, X[i],Y[i],kz,Fx[i]
           print >> file_y, X[i],Y[i],kz,Fy[i]
           print >> file_z, X[i],Y[i],kz,Fz[i]
        y_old = Y[i]
      file_x.close()
      file_y.close()
      file_z.close()


class berry_dipole:
  """Methods to compute the Berry dipole starting from a Berry curvature calculation """

  def __init__(self,**kwargs):
      self.nx = kwargs['nx']
      self.ny = kwargs['ny']
      self.nz = kwargs['nz']
      self.points = []
      self.root_kz = kwargs['root_kz']
      l = len(self.root_kz)
      self.kz_names = kwargs.pop('kz_names',[name for name in os.listdir(".") if os.path.isdir(name) and name[0:l]==self.root_kz])
      self.kz_names.sort(key=natural_keys)
      print "Folders with different kz: ", self.kz_names,"\n"

      #read reciprocal lattice vectors
      kz_fold = self.kz_names[0]
      wpout_file = """%(kz_fold)s/%(kz_fold)s.wpout"""%locals()
      file = open(wpout_file,'r')
      data = file.readlines()
      file.close()

      units = None
      for ind in range(len(data)-1,0,-1):  #going backward to read the last calculation

        if "b_1" in data[ind]:
          units = data[ind-1]
          vals = map(eval,data[ind].split()[1:4])
          self.b_1 = [vals[0],vals[1],vals[2]]

          vals = map(eval,data[ind+1].split()[1:4])
          self.b_2 = [vals[0],vals[1],vals[2]]

          vals = map(eval,data[ind+2].split()[1:4])
          self.b_3 = [vals[0],vals[1],vals[2]]
          break
      if(units == None):
         print "Check your wpout file. Problem reading reciprocal lattice vectors and/or units"
      else:
         print units
         print "b_1: ", self.b_1, "\n"
         print "b_2: ", self.b_2, "\n"
         print "b_3: ", self.b_3, "\n"


  def split_kz_berry_curvature(self,**kwargs):

      file_name = kwargs['file_name']
      root_output = kwargs.pop('root_output','outsplit')

      for kz_fold in self.kz_names:
        ints = re.findall('\d+', kz_fold)
        kz = eval(ints[0])*1./eval(ints[1])*self.b_3[2]
        split_berry_curvature(file_name=file_name,folder_name=kz_fold,kz=kz,root_output=root_output)

  def generate_energy_file(self,**kwargs):

      file_band = kwargs['file_band']
      file_output = kwargs.pop('file_output','energy.dat')

      for kz_fold in self.kz_names:

         file_name = "%s-%s"%(kz_fold,file_band)

         file = open("""%(kz_fold)s/%(file_name)s"""%locals(),'r')
         data = file.readlines()
         file.close()

         file = open("%s/%s"%(kz_fold,file_output),'w')
         for fila in data:
           vals = fila.split()
           if(len(vals)>2):
             print >> file, fila,
         file.close()


  def bd_component(self,**kwargs):
      """ Main function of this class: computes the derivative of the Berry curvature along the kx or ky directions, and perform its integration.

          Input variables:
          - f_index: index of function 0,1,2 are Omega_x, Omega_y and Omega_z in the curve file provided by wannier90 
          - coor_index: coordinate with respect the derivative is done: 0,1 > kx,ky
          - delta_k: amplitude of the displacement
          - energy_limit: energy limit to consider for the derivatives. If not specified, is set to zero.
          - kz_names: list of kz names to use. If not specified, self.kz_names is used. The idea is to avoid planes where there is no fermi surface.
          - input_curv: file from where to read the Berry curvature. If not specified, it uses kz_fold-kslice-curv.dat
          - extra_points: boolean that indicates if k-points fabricated by summing reciprocal lattice vectors should be added.

          It requires energy.dat file to be present in each of the kz folders. This file should contain the energy of the band close to Fermi level.
      """

      f_index = kwargs['f_index']
      coor_index = kwargs['coor_index']
      delta_k = kwargs['delta_k']
      energy_limit = kwargs.pop('energy_limit',0.0)
      extra_points = kwargs.pop('extra_points',False)
      kz_names = kwargs.pop('kz_names',self.kz_names)
      input_curv = kwargs.pop('input_curv',None)
      input_coor = kwargs.pop('input_coor',None)
 
      der_vals = ["x","y"]
      func_vals = ["Omx","Omy","Omz"]
      der_val = der_vals[coor_index]
      func_val = func_vals[f_index]
      file_output_name = """der_%(func_val)s_%(der_val)s_deltak_%(delta_k)s"""%locals()
      file_output = open(file_output_name,'w')
      b_1 = [self.b_1[0],self.b_1[1]]
      b_2 = [self.b_2[0],self.b_2[1]]

      #displacement for derivative calculation
      if(coor_index == 1):
          displacement = [0,delta_k]
      if(coor_index == 0):
          displacement = [delta_k,0]
      if( not(coor_index == 0 or coor_index == 1)):
          print 'Check coor_index'
          assert 0

      #volume element for integration
      V = abs(self.b_3[0] * ( self.b_1[1]*self.b_2[2] - self.b_1[2]*self.b_2[1]) - self.b_3[1] * ( self.b_1[0]*self.b_2[2] - self.b_1[2]*self.b_2[0])  +  self.b_3[2] * ( self.b_1[0]*self.b_2[1] - self.b_1[1]*self.b_2[0]))
      dV = V / (self.nx * self.ny * self.nz)
      print >> file_output, "Volume: ", V, " dV:", dV
       
      #Reading coordiantes: it is assumed that the in-plane coordinates are the same along kz.
      print >> file_output, "Read coordinates"
      kz_fold = kz_names[0]
      if(input_coor == None):
        coord_file = """%(kz_fold)s/%(kz_fold)s-kslice-coord.dat"""%locals()
      else:
        coord_file = """%(kz_fold)s/%(input_coor)s"""%locals()
      file = open(coord_file,'r')
      data_coor = file.readlines()
      file.close()
 
      print >> file_output, "Now considering data from: ", kz_names
      sum_total = 0.0
      for kz_fold in kz_names:
          print >> file_output, "Reading curvature and energy: ", kz_fold
          output_der_file = """%(kz_fold)s/aux_f_%(f_index)s_v_%(coor_index)s """%locals() #output file for extra info on derivative

          #Reading curvature
          if(input_curv == None):
             curv_file = """%(kz_fold)s/%(kz_fold)s-kslice-curv.dat"""%locals()
          else:
             curv_file = """%(kz_fold)s/%(input_curv)s"""%locals()
          file = open(curv_file,'r')
          data_curv = file.readlines()
          file.close()

          #Reading energy
          ene_file = """%(kz_fold)s/energy.dat"""%locals()
          file = open(ene_file,'r')
          data_ene = file.readlines()
          file.close()

          #Build list of points whose energy is below the Fermi level 0
          pxy = []
          function = []
          energies = []
          n_e = 0
          for i in range(len(data_coor)-1): #avoid last empty line present in output of wannier90 files

             #coordinates
             fila = data_coor[i]
             vals_coor = map(eval,fila.split())
             kx,ky = vals_coor[0],vals_coor[1]

             #curvature
             fila = data_curv[i]
             vals_curv = map(eval,fila.split())
             f = vals_curv[f_index]

             #energy
             fila = data_ene[i]
             vals_ene = map(eval,fila.split())
             e = vals_ene[-1]
             if(e<0):
               n_e +=1
          
             pxy.append([kx,ky])
             function.append(f)
             energies.append(e)

          #consider possibility of adding additional points (probably this will be deprecated very soon)
          total_pxy = [a for a in pxy]
          total_function = [a for a in function]
          total_ene = [a for a in energies]
 
          print >> file_output, "Lenght of pxy,function,energies: ", len(pxy), len(function), len(energies)
          if(extra_points):
            print >> file_outpout, "Adding extra points"
            extra_p = []
            extra_f = []
            extra_ene = []
            i = 0
            xc = 0.5 * (b_1[0] + b_2[0])
            yc = 0.5 * (b_1[1] + b_2[1])
            norm = xc**2 + yc**2
            for p in pxy:
               for c_i in [-1,0,1]:
                 for c_j in [-1,0,1]:
                   if(not(c_i == 0 and c_j ==0)):
                     x = p[0] + c_i * b_1[0] + c_j * b_2[0]
                     y = p[1] + c_i * b_1[1] + c_j * b_2[1]
                     if((x-xc)**2+(y-yc)**2 < 1.3*norm):
                       extra_p.append([x,y])
                       extra_f.append(function[i])
                       extra_ene.append(energies[i])

               i+=1

         
            for i in range(len(extra_p)):
               total_pxy.append(extra_p[i])
               total_function.append(extra_f[i])
               total_ene.append(extra_ene[i])
    
            print >> file_output, "Length after adding extra points: tot pxy, functin,energies ", len(total_pxy), len(total_function), len(total_ene)
         
          if(n_e == 0):
            print >> file_output,"kz ", kz_fold, " : No contribution within fermi surface"
          else: 
            der = derive_in_plane(displacement=displacement,points=total_pxy,function=total_function,energy=total_ene,file_output=output_der_file,energy_limit=energy_limit)

            sum_partial = 0
            for contr in der:
               sum_partial += contr
            sum_total += sum_partial * dV
            print >> file_output,"sums:", sum_partial, sum_total

      file_output.close()


  def substract(self,sub_folder,output_name):
      """
      Function that substract the Berry curvature calculated with two different chemical potential.
      sub_folder: address of the data to substract. It should be organized exactly as in self.kz_names
      output_name: file where the result is saved. It will have the same format than -kslice-curv.dat data from wannier90
      """
      for kz_fold in self.kz_names:

          curv_file = """%(kz_fold)s/%(kz_fold)s-kslice-curv.dat"""%locals()
          file = open(curv_file,'r')
          data_orig = file.readlines()
          file.close()

          curv_file = """%(sub_folder)s/%(kz_fold)s/%(kz_fold)s-kslice-curv.dat"""%locals()
          file = open(curv_file,'r')
          data_to_subtract = file.readlines()
          file.close()

          if(len(data_orig) == len(data_to_subtract)):
            curv_file = """%(kz_fold)s/%(output_name)s"""%locals()
            file_output = open(curv_file,'w')

            for i in range(len(data_orig)-1):
               fila_orig = data_orig[i].split()
               fila_to_subtract = data_to_subtract[i].split()
               vals_orig = map(eval,fila_orig)
               vals_to_subtract = map(eval,fila_to_subtract)
               print >> file_output,vals_orig[0]-vals_to_subtract[0], vals_orig[1]-vals_to_subtract[1], vals_orig[2]-vals_to_subtract[2]
             
            file_output.close()
          else:
            print 'Check input files in kz_fold: ',kz_fold





