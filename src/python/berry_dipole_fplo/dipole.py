#!/usr/bin/python

import sys,os,re,string,shutil
from subprocess import call
import numpy as np
import numpy.linalg as LA
import math

import pyfplo.slabify as sla
from scipy.spatial import Delaunay

def dfermi_dmu(x):
   return np.exp(np.float128(x)) / (1+np.exp(np.float128(x)))**2


def histogram_berry(**kwargs):
  folder_root = kwargs["folder_root"]
  file_name = kwargs["file_name"]
  TOL = kwargs.pop("TOL",1e-9)
  range_x = kwargs["range_x"]
  range_y = kwargs["range_y"]
  delta = kwargs["delta"]
  folders = []
  for can in os.listdir(os.getcwd()):
     if(can.startswith(folder_root)):
        folders.append(can)

  omx = []
  omy = []
  omz = []
  kps_number = []
  Nx = int((range_x[1]-range_x[0])/delta)
  Ny = int((range_y[1]-range_y[0])/delta)
  for i in range(Nx):
    rx= []
    ry= []
    rz= []
    kp= []
    for j in range(Ny):
       rx.append(0.0)
       ry.append(0.0)
       rz.append(0.0)
       kp.append(0.0)
    omx.append(rx)
    omy.append(ry)
    omz.append(rz)
    kps_number.append(kp)

  for folder in folders:
     name = """%(folder)s/%(file_name)s"""%locals()
     try: 
       file = open(name,'r')
       data = file.readlines()
       file.close()

       for fila in data:
         vals = map(eval,fila.split())
         if(vals[0] >range_x[0] and vals[0] < range_x[1] and vals[1] >range_y[0] and vals[1] < range_y[1]):  
           ind_x = int((vals[0]-range_x[0])/delta)
           ind_y = int((vals[1]-range_y[0])/delta)
           omx[ind_x][ind_y] += vals[3]
           omy[ind_x][ind_y] += vals[4]
           omz[ind_x][ind_y] += vals[5]
           kps_number[ind_x][ind_y] += 1
         else:
           print "Warning: point outside ranges in folder: ",folder
     except:
         "Folder not contributing: ", folder
  file = open("histogram.dat",'w')
  for i in range(Nx):
     for j in range(Ny):
       print >> file,i*delta+range_x[0],j*delta+range_y[0],omx[i][j],omy[i][j],omz[i][j],kps_number[i][j]

def average_berry(**kwargs):

  folder_root = kwargs["folder_root"]
  file_name = kwargs["file_name"]
  TOL = kwargs.pop("TOL",1e-9)
  folders = []
  for can in os.listdir(os.getcwd()):
     if(can.startswith(folder_root)):
        folders.append(can)

  list_of_kxky = []
  list_of_averages = []

  for folder in folders:
     name = """%(folder)s/%(file_name)s"""%locals()
     try: 
       file = open(name,'r')
       data = file.readlines()
       file.close()

       for fila in data:
           vals = map(eval,fila.split())
           kx = vals[0]
           ky = vals[1]
           point_already_in = False
           i = 0
           for kp in list_of_kxky:
              if((kx-kp[0])**2 + (ky-kp[1])**2 < TOL):
                 point_already_in = True
                 ind_point = i
                 break
              i +=1
           if(point_already_in):
              list_of_averages[ind_point].append(vals[3:6])
           else:
              list_of_kxky.append([kx,ky])
              list_of_averages.append([vals[3:6]])


     except IOError:
       print "Folder not contributing: ",folder 

  file = open("BAverage.dat","w")

  for i in range(len(list_of_kxky)):
     kp = list_of_kxky[i]
     vals = list_of_averages[i]
     av_x = 0
     av_y = 0
     av_z = 0
     for el in vals:
       av_x += el[0]
       av_y += el[1]
       av_z += el[2]
     av_x = av_x/len(vals)
     av_y = av_y/len(vals)
     av_z = av_z/len(vals)
     print >> file, kp[0],kp[1],av_x,av_y,av_z,len(vals)
  file.close()
   
  print folders
  

def anomalous_nernst(**kwargs):
  """
    Computes anomalous Nernst conductivity as in Phys. Rev. Lett. 97, 026603 Eq. 8
  
    Requires:
    - file_name: DEPRECATED file with the anomalous Hall at zero temperature as a function of energy.
    - betas: list [beta_0,beta_1,...] range of temperatures for which the calculation will be performed (given as 1/kbT, in 1/eV).
    - energy_fermi: list [mu_0,mu_1,...] of chemical potential for which the calculation will be performed
  """
  #file_name = kwargs['file_name']
  betas = kwargs['betas']
  energy_fermi = kwargs['energy_fermi']

  #read anomalous hall xy,xz,yz 
 
  file = open("sigma_xy_vs_mu.dat")
  data_xy = file.readlines()
  file.close()
  file = open("sigma_xz_vs_mu.dat")
  data_xz = file.readlines()
  file.close()
  file = open("sigma_yz_vs_mu.dat")
  data_yz = file.readlines()
  file.close()

  hall_data = [data_xy,data_xz,data_yz]
  labels = ["xy","xz","yz"]
  #read energies
  energies = []
  for fila in data_xy:
     vals = map(eval,fila.split())
     energies.append(vals[0])  

  kb = 1.38064852 #10**-23 J/K (boltzmann constant)
  A = 6.25 #10**18 e/s (Ampere)
  
  unit_factor = kb * A *0.001 #the las factor is the result of 10**(-23+18+2) = 10**-3, where the +2 is for conversing cm to m

  case = 0
  for data in hall_data:
    anom_hall = []
    label = labels[case]
    for fila in data:
       vals = map(eval,fila.split())
       anom_hall.append(vals[1])  

    for mu in energy_fermi:
       file_mu = open("""anomalous_nernst_%(label)s_%(mu)s"""%locals(),'w')
       """ The format of this file is: 
           beta[eV], nernst[A/(meter K)]
 
       """
       print >> file_mu, "#beta[eV], nernst conductivity[A/(mK)]"
       for beta in betas:
          file_muint = open("""diff_anomalous_nernst_%(label)s_%(mu)s_beta_%(beta)s"""%locals(),'w')
          e_0 = energies[0]
          e_1 = energies[1]
          anom_nernst = 0
#          i = 0
#          for eps in energies:

#             de = e_1-e_0
      
#             x = beta * (eps-mu)
#             anom_nernst += -de * anom_hall[i] * x * dfermi_dmu(x) * beta
             
#             print >> file_muint, eps, anom_hall[i] * x * dfermi_dmu(x) * beta
#             i+=1
#             e_0 = energies[i-1]
#             if(i< len(energies)):
#               e_1 = energies[i]
#          file_muint.close()
          
          for i in range(len(energies)-1):
             de = energies[i+1]-energies[i] 
             eps = 0.5 * (energies[i+1]+energies[i])
             x = beta * (eps-mu)
             av_hall = 0.5 * (anom_hall[i+1]+anom_hall[i])
             anom_nernst += -de * av_hall * x * dfermi_dmu(x) * beta * unit_factor
             print >> file_muint, eps-mu, anom_hall[i] * x * dfermi_dmu(x) * beta * unit_factor
          file_muint.close()

          print >> file_mu, beta, anom_nernst

       file_mu.close()
    case +=1

def hacerfig(file_name):

  file = open(file_name)
  data = file.readlines()
  file.close()

  for line in data:
        aux = line.find('epsfile')
        if (aux == 0):
                filename = (line[9:-2])[:-4]
                break

  call(['gnuplot' ,file_name])

  texfiledata =""" 
\documentclass[prl,10pt]{revtex4}
\usepackage[dvips]{graphicx}

\\begin{document}
\\thispagestyle{empty}

\\begin{figure}
\\input{"%(filename)s"}
\\end{figure} 

\\end{document}
"""%locals()

  file = open("aux.tex",'w')
  file.write(texfiledata)
  file.close()

  call(['latex', 'aux.tex'])
  call(['dvips', "aux.dvi" ])
  call(['ps2epsi', "aux.ps" ])

  os.remove("aux.tex")
  os.remove("aux.dvi")
  os.remove("aux.ps")
  os.remove("aux.log")
  os.remove("aux.aux")
  os.remove(filename+".tex")
  shutil.move("aux.epsi", filename+".eps")
  call(['ps2pdf',  "-dEPSCrop",  filename+".eps"])

class berry_dipole:
  """
   Class for handling Berry dipole calculations
  """
 
  def __init__(self,**kwargs):
      """
      Arguments:
       - slabify object
       - energy_bottom: bottom energy
       - energy_fermi: [ef_1,..,ef_n] 
       - ms: parameter need by hamAtKPoint
       - delta: displacement to perform the numerical derivative.
      """
      self.root_name = kwargs.pop('root_name','out')
      self.S = kwargs['slabify']
      self.delta = kwargs.pop('delta',1e-5)
      self.ms  = kwargs['ms']
      self.energy_bottom = kwargs['energy_bottom']
      self.energy_fermi = kwargs['energy_fermi']
      self.mesh = kwargs['mesh']
      R=self.S.hamdataCCell()
      G=self.S.hamdataRCell()
      print 'R= [Unit: Bohr]\n',R
      print 'G= [Unit: fplo]\n',G
      self.G1=G[:,0]
      self.G2=G[:,1]
      self.G3=G[:,2]
      print 'Reciprocal lattice vectors: [Bohr^{-1}] ',
      print 'G1: ', self.G1*self.S.kscale
      print 'G2: ', self.G2*self.S.kscale
      print 'G3: ', self.G3*self.S.kscale
      self.dG1=LA.norm(self.G1)
      self.dG2=LA.norm(self.G2)
      self.dG3=LA.norm(self.G3)
      self.bohr_to_cm = 5.29177e-9     
      self.G_0 = 1./12906.4037217 #ohm-1 (conductance quantum: 2e^2/h)
      self.VOL_fplo = np.abs(np.linalg.det(G))
      self.VOL_bohr = np.abs(np.linalg.det(G))*(self.S.kscale)**3

      print "\n Volume of real space primitive cell [Bohr^3]: ",np.abs(np.linalg.det(R)), "\n"
      print "\n Volume of primitve cell [fplo^3]: ",self.VOL_fplo, "\n"
      print "\n Volume of primitve cell [Bohr^-3]: ",self.VOL_bohr,"\n"
      print "\n |G_i|: ",self.dG1,self.dG2,self.dG3,"\n"
      print "\n Mesh: ",self.mesh, "\n"
      print "\n Energy bottom: ",self.energy_bottom, "\n"
      print "\n Fermi energies considered: ",self.energy_fermi, "\n"

  def build_default_3D_box(self):
      box=sla.BoxMesh()
      box.setBox(xaxis=self.G1,yaxis=self.G2,zaxis=self.G3,origin=[0,0,0])
      box.setMesh(nx=self.mesh[0],xinterval=[-0.5*self.dG1,(0.5-1./self.mesh[0])*self.dG1],
                  ny=self.mesh[1],yinterval=[-0.5*self.dG2,(0.5-1./self.mesh[1])*self.dG2],
                  nz=self.mesh[2],zinterval=[-0.5*self.dG3,(0.5-1./self.mesh[2])*self.dG3])

      #kpoints = kwargs.pop("kpoints",box.mesh(self.S.kscale)) #after this the points are in Bohr
      kpoints = box.mesh(self.S.kscale) #after this the points are in Bohr
      return kpoints

  def berry_curvature_plane(self,**kwargs):
      """
      Compute Berry curvature on a set of points and save to a file
      """

      kpoints_from_file = kwargs.pop("kpoints_from_file",False)
      folder_file = kwargs.pop("folder_file","./")
      kpoints = kwargs.pop('kpoints',[])
      out_name = kwargs.pop("out_name","berry.dat")
      cutoff = kwargs.pop("cutoff",1e25)

      if(kpoints_from_file):
        namefile = "%s/klist.dat"%(folder_file)
        file = open(namefile,'r')
        data = file.readlines() 
        file.close()
        kpoints = []
        for fila in data:
            vals = map(eval,fila.split())
            kpoints.append([vals[0],vals[1],vals[2]])
            
      total_omega = []
      for kp in kpoints:
        #compute Hamiltonian eigenenergies and Berry curvature at kp
        k = np.array(kp)
        (Hk,dHk) = self.S.hamAtKPoint(k,self.ms,gauge='periodic',makedhk=True)
        (E,CC,F) = self.S.diagonalize(Hk,dhk=dHk,makef=True)
        ind = 0
        Omega_kp =  [ [0, 0, 0] for e in self.energy_fermi] #one per energy Fermi considered 
        for e in E:
          for ind_ef in range(len(self.energy_fermi)):
           if(self.energy_bottom <= e <= self.energy_fermi[ind_ef]):
             if(np.sqrt(F[0][ind]**2 + F[1][ind]**2 + F[2][ind]**2) < cutoff):
               Omega_kp[ind_ef][0] += F[0][ind]
               Omega_kp[ind_ef][1] += F[1][ind]
               Omega_kp[ind_ef][2] += F[2][ind]
             else:
               print "WARN: Large Berry curvature (larger than cutoff) at kp: ", kp
          ind +=1
        total_omega.append(Omega_kp)

      for ind_ef in range(len(self.energy_fermi)):
         name = """%(out_name)s_%(ind_ef)s"""%locals()
         file = open(name,'w')

         for i in range(len(kpoints)):
             k = kpoints[i]
             print >> file, k[0],k[1],k[2],total_omega[i][ind_ef][0],total_omega[i][ind_ef][1],total_omega[i][ind_ef][2]

         file.close()


  def dipole_2D(self):
      kz_plane = 0
      origin = [0,0,0]
      cutoff = 1e25
      derivative_cases = [[2,0],[2,1]]

      box=sla.BoxMesh()
      box.setBox(xaxis=self.G1,yaxis=self.G2,zaxis=self.G3,origin=origin)
      box.setMesh(nx=self.mesh[0],xinterval=[-0.5*self.dG1,(0.5-1./self.mesh[0])*self.dG1],
                  ny=self.mesh[1],yinterval=[-0.5*self.dG2,(0.5-1./self.mesh[1])*self.dG2],
                  nz=1,zinterval=[kz_plane,kz_plane])

      #kpoints = kwargs.pop("kpoints",box.mesh(self.S.kscale)) #after this the points are in Bohr
      kpoints = box.mesh(self.S.kscale) #after this the points are in Bohr
      DK = self.dG1 * self.dG2 / (self.mesh[0]*self.mesh[1])
      Omega_integral = [ 0  for e in self.energy_fermi] #one per energy Fermi considered
      Dipole_integral = [[0 for i in range(len(derivative_cases))]  for e in self.energy_fermi]

      for kp in kpoints:

        #compute Hamiltonian eigenenergies and Berry curvature at kp
        k = np.array(kp)
        (Hk,dHk) = self.S.hamAtKPoint(k,self.ms,gauge='periodic',makedhk=True)
        (E,CC,F) = self.S.diagonalize(Hk,dhk=dHk,makef=True)

        #Anomalous Hall:
        ## First integrate over bands at this k-point
        Omega_z_kp =  [ 0. for e in self.energy_fermi] #one per energy Fermi considered 
        ind = 0
        for e in E:
          for ind_ef in range(len(self.energy_fermi)):
           if(self.energy_bottom <= e <= self.energy_fermi[ind_ef]):
             if(np.sqrt(F[0][ind]**2 + F[1][ind]**2 + F[2][ind]**2) < cutoff):
               Omega_z_kp[ind_ef] += F[2][ind]
             else:
               print "WARN: Large Berry curvature (larger than cutoff) at kp: ", kp
          ind +=1
        ## Second, contribute to sum in k at each Fermi energy
        for ind_ef in range(len(self.energy_fermi)):
           Omega_integral[ind_ef] += Omega_z_kp[ind_ef]*DK

        #Berry dipole
        i_case = 0
        for case in derivative_cases:

             ## First, integrate over bands at this k-pint

             ### evaluate displaced k-point for numerical derivative
             k_disp = np.copy(k)
             k_disp[case[1]] += self.delta
             #print >> file,k[0],k[1],k[2],k_disp[0],k_disp[1],k_disp[2]

             ### evaluate displaced Berry curvature
             (Hk_disp,dHk_disp) = self.S.hamAtKPoint(k_disp,self.ms,makedhk=True)
             (E_disp,CC_disp,F_disp) = self.S.diagonalize(Hk_disp,dhk=dHk_disp,makef=True)

             ### evaluate finite difference
             deriv_case_kp = [0. for e in self.energy_fermi] #one per energy Fermi considered
             #file = open("acum_kp_%s"%(i_kp),'w')
             ind = 0 #band index
             for e in E:
               for ind_ef in range(len(self.energy_fermi)):
                 if(self.energy_bottom <= e <= self.energy_fermi[ind_ef]):
                   if(np.abs(F_disp[case[0]][ind]-F[case[0]][ind]) < cutoff):
                     #print >> file, ind, deriv[ind_ef], F[case[0]][ind],F_disp[case[0]][ind]
                     deriv_case_kp[ind_ef] += (F_disp[case[0]][ind]-F[case[0]][ind]) / self.delta           
                   else:
                     #print >>file, ind,"#WARN: Large finite difference at kp: ", k,k_disp, F[case[0]][ind],F_disp[case[0]][ind]
                     print "#WARN: Large finite difference at kp: ", k,ind, F[case[0]][ind],F_disp[case[0]][ind]
               ind +=1
               #file.close()
 
             ## Second, contribute to sum in k at each Fermi energy
             for ind_ef in range(len(self.energy_fermi)):
                Dipole_integral[ind_ef][i_case] += deriv_case_kp[ind_ef] * DK
             i_case +=1


      print "\n -- Omega_integral written "
      if(len(derivative_cases)>0):
          print "\n -- Dipole_integral written "

      file_om = open("omega_vs_mu.dat",'w')
      file_dip = open("dip_vs_mu.dat",'w')
      for ind_ef in range(len(self.energy_fermi)): #one per energy Fermi considered
        print >> file_om,self.energy_fermi[ind_ef],Omega_integral[ind_ef]
        print >> file_dip, self.energy_fermi[ind_ef], Dipole_integral[ind_ef][0], Dipole_integral[ind_ef][1]
      file_om.close()
      file_dip.close()

    

  def berry_slice(self,**kwargs):
      """
        Main function: Computes de Berry curvature dipole in a given plane

        Arguments:
        - plane: integer corresponding to the plane
        - output_file: name of output file. By default is: "out_berry_%(plane)" + "out_dipole_%(plane)"
        - derivative_cases: list of derivatives to compute. The notation of each element of the list is: [i,j], meaning \partial Omega_i / \partial k_j
        - print_berry_data: boolean. True if one needs to print the Berry curvature and Berry dipole data. By default is False

        Optional arguments:
        - cutoff: neglects Berry curvatures or diferences larger than these value
        - origin: origin of the box for samplint kpoints

        Returns:
        - A list with [total_omega,total_derivative]
        Print total_omega and total_derivative to output files

      """
      """
        Objects involved in the following lines:
        - Omega_integral: (STD output) a summation in k of the total Berry curvature (without prefactors, just the bare summatio).
        - Dipole_integral: (STD output) analogous to the previous.
        - total_omega: (Return by hand and to file) list of total Berry curvature (total in the sense that sums in energy, but it has one element per k-point)
        - total_derivative: (Return by hand and to file) list of total Berry curvature derivatives
        - Omega_kp: (auxilar) At each k-point, total Berry curvature 
        - derivatives: (auxiliar) At each k-pint, list of derivativs to be computed      
        - deriv: (auxiliar) At each k-point, each derivative to be compute
      """

      plane = kwargs["plane"]
      origin = kwargs.pop("origin",[0.,0.,0.])
      print "Origin: ",origin
      force_c4v = kwargs.pop("force_c4v",False)
      cutoff = kwargs.pop("cutoff",1e25)
      output_file = [kwargs.pop("out_berry","out_berry_%s"%(plane)),kwargs.pop("out_dipole","out_dipole_%s"%(plane))]
      derivative_cases = kwargs.pop("derivative_cases",[])
      print_berry_data = kwargs.pop("print_berry_data",False)

      #default kpoint box
      kz_plane = (-0.5 + plane*1./self.mesh[2])*self.dG3
      box=sla.BoxMesh()
      box.setBox(xaxis=self.G1,yaxis=self.G2,zaxis=self.G3,origin=origin)
      box.setMesh(nx=self.mesh[0],xinterval=[-0.5*self.dG1,(0.5-1./self.mesh[0])*self.dG1],
                  ny=self.mesh[1],yinterval=[-0.5*self.dG2,(0.5-1./self.mesh[1])*self.dG2],
                  nz=1,zinterval=[kz_plane,kz_plane])

      #kpoints = kwargs.pop("kpoints",box.mesh(self.S.kscale)) #after this the points are in Bohr
      kpoints = box.mesh(self.S.kscale) #after this the points are in Bohr

      file = open("plane_kpoints",'w')
      for p in kpoints:
        print >> file, p[0],p[1],p[2]
      file.close()

      print "Doing berry calculation for plane: ", plane
      print "Number of kpoints: ", len(kpoints)
      print "Output files: ", output_file
      print "kz_plane: ",kz_plane

      Area_integral = self.area_slice(points=kpoints,write_plane=True)
      Omega_integral = [[0,0,0]  for e in self.energy_fermi] #one per energy Fermi considered

      Dipole_integral = [[0 for i in range(len(derivative_cases))]  for e in self.energy_fermi]

      total_omega = []
      total_derivatives = [] #at each kpoint, a list of all the derivatives (one per case in cases)
      for kp in kpoints:

        #compute Hamiltonian eigenenergies and Berry curvature at kp
        k = np.array(kp)
        (Hk,dHk) = self.S.hamAtKPoint(k,self.ms,gauge='periodic',makedhk=True)
        (E,CC,F) = self.S.diagonalize(Hk,dhk=dHk,makef=True)
        ind = 0

        #Anomalous Hall:
        Omega_kp =  [ [0, 0, 0] for e in self.energy_fermi] #one per energy Fermi considered 
        for e in E:
          for ind_ef in range(len(self.energy_fermi)):
           if(self.energy_bottom <= e <= self.energy_fermi[ind_ef]):
             if(np.sqrt(F[0][ind]**2 + F[1][ind]**2 + F[2][ind]**2) < cutoff):
               Omega_kp[ind_ef][0] += F[0][ind]
               Omega_kp[ind_ef][1] += F[1][ind]
               Omega_kp[ind_ef][2] += F[2][ind]
             else:
               print "WARN: Large Berry curvature (larger than cutoff) at kp: ", kp
          ind +=1
        total_omega.append(Omega_kp)

        for ind_ef in range(len(self.energy_fermi)):
          for i in range(3):
             Omega_integral[ind_ef][i] += Omega_kp[ind_ef][i]

        #now compute required derivatives
        if(len(derivative_cases)>0):
          derivatives = [] #accumulated derivative (one per case in derivative_cases)
          i_case = 0
          for case in derivative_cases:
             #displaced k-point
             k_disp = np.copy(k)
             k_disp[case[1]] += self.delta
       #      print >> file,k[0],k[1],k[2],k_disp[0],k_disp[1],k_disp[2]
             #displaced Berry curvature
             (Hk_disp,dHk_disp) = self.S.hamAtKPoint(k_disp,self.ms,makedhk=True)
             (E_disp,CC_disp,F_disp) = self.S.diagonalize(Hk_disp,dhk=dHk_disp,makef=True)

             #finite difference
             ind = 0 #band index
             deriv = [0 for e in self.energy_fermi] #one per energy Fermi considered
#             file = open("acum_kp_%s"%(i_kp),'w')
             for e in E:
               for ind_ef in range(len(self.energy_fermi)):
                 if(self.energy_bottom <= e <= self.energy_fermi[ind_ef]):
                   if(np.abs(F_disp[case[0]][ind]-F[case[0]][ind]) < cutoff):
#                    print >> file, ind, deriv[ind_ef], F[case[0]][ind],F_disp[case[0]][ind]
                     deriv[ind_ef] += (F_disp[case[0]][ind]-F[case[0]][ind]) / self.delta           
                   else:
                     #print >>file, ind,"#WARN: Large finite difference at kp: ", k,k_disp, F[case[0]][ind],F_disp[case[0]][ind]
                     print "#WARN: Large finite difference at kp: ", k,ind, F[case[0]][ind],F_disp[case[0]][ind]
               ind +=1
#             file.close()
             derivatives.append(deriv)
 
             for ind_ef in range(len(self.energy_fermi)):
                Dipole_integral[ind_ef][i_case] += deriv[ind_ef]
             i_case +=1

          total_derivatives.append(derivatives)
      #file.close()
      if(print_berry_data):
         ind_ef = 0 #int(len(self.energy_fermi)/2) #plot berry curvature of first fermi energy in self.fermi_energy
         file_om = open(output_file[0],'w')
         if(len(derivative_cases)>0):
           file_der = open(output_file[1],'w')
         for i in range(len(kpoints)):
             k = kpoints[i]

             #omega output
             print >> file_om, k[0],k[1],k[2],total_omega[i][ind_ef][0],total_omega[i][ind_ef][1],total_omega[i][ind_ef][2]

             #dipole output
             if(len(derivative_cases)>0):
               print >> file_der, k[0],k[1],k[2],
               for j in range(len(derivative_cases)):
                  print >> file_der, total_derivatives[i][j][ind_ef],
               print >> file_der, "\n", 

         file_om.close()

      print "\n -- Area_integral: ",Area_integral
      print "\n -- Omega_integral written "
      if(len(derivative_cases)>0):
          print "\n -- Dipole_integral written "

      root = self.root_name
      if(not os.path.isdir("""%(root)s_%(plane)s"""%locals())):
        os.mkdir("""%(root)s_%(plane)s"""%locals())
      file_om = open("""%(root)s_%(plane)s/omega_vs_mu.dat"""%locals(),'w')
      for ind_ef in range(len(self.energy_fermi)): #one per energy Fermi considered
        print >> file_om,self.energy_fermi[ind_ef],
        for i in range(3):
           print >> file_om,Omega_integral[ind_ef][i],
        print >> file_om,"\n",
      file_om.close()
      if(len(derivative_cases)>0):
          file_dip = open("""%(root)s_%(plane)s/dip_vs_mu.dat"""%locals(),'w')
          for ind_ef in range(len(self.energy_fermi)):
            print >> file_dip, self.energy_fermi[ind_ef],
            for i in range(len(derivative_cases)):
               print >> file_dip,Dipole_integral[ind_ef][i],
            print >> file_dip,"\n",
          file_dip.close()
          if(print_berry_data):
            file_der.close()
          return [total_omega,total_derivatives]
      else:
          return total_omega

  def integrate_slices(self,**kwargs):
      """
        Main function: Computes de Berry curvature dipole in a given plane

        Arguments:
        - mesh: mesh
        - file_name: name of the file to be read in the folders. By default file_name=root_name

        Returns:
        - Integrals

      """

      number_cases = kwargs.pop("number_cases",0)
      derivative_cases = kwargs.pop("derivative_cases",0)
      file_name = self.root_name

      files = ["""%(file_name)s_%(i)s/%(file_name)s_%(i)s"""%locals() for i in range(self.mesh[2])]
      files_om = ["""%(file_name)s_%(i)s/omega_vs_mu.dat"""%locals() for i in range(self.mesh[2])]
      files_dip = ["""%(file_name)s_%(i)s/dip_vs_mu.dat"""%locals() for i in range(self.mesh[2])]

      Om_x = [0 for e in self.energy_fermi]
      Om_y = [0 for e in self.energy_fermi]
      Om_z = [0 for e in self.energy_fermi]
      dipole = [[0 for i in range(number_cases)] for e in self.energy_fermi]
      dk = self.VOL_bohr / (self.mesh[0]*self.mesh[1]*self.mesh[2] )/(2*np.pi)**3

      #integrate area
      #construct dk_perp: builds normal vector of plane formed by G1 and G2, and then project G3 on this vector
      aux_3 = np.cross(self.G1,self.G2)
      aux_3_norm = LA.norm(aux_3)
      aux_3 = aux_3 / aux_3_norm
      dk_perp = abs(np.dot(self.G3,aux_3)) /self.mesh[2] * self.S.kscale

       
      #integrate area
      aux_vol = 0
      for name in files:
         file = open(name,'r')
         data = file.readlines()
         file.close()
         for fila in data:
            if "Area_integral" in fila:
               svals = fila[20:]
               vals = map(eval,svals.split())
               aux_vol += vals[0] * dk_perp

      #integrate Berry curvature
      for name in files_om:
         sum_to_omega = False
         file = open(name,'r')
         data = file.readlines()
         file.close()
         assert(len(data) == len(self.energy_fermi))
         ind_ef = 0 
         for fila in data:
            vals = map(eval,fila.split())
            Om_x[ind_ef] += vals[1] * dk
            Om_y[ind_ef] += vals[2] * dk
            Om_z[ind_ef] += vals[3] * dk
            sum_to_omega = True
            ind_ef +=1
         if(not sum_to_omega):
            print "WARNING: folder ",name, " is not contributing to Berry curvature integration"     

      #integrate Berry curvature dipole
      if(number_cases>0):
       for name in files_dip:
         sum_to_dipole = False
         file = open(name,'r')
         data = file.readlines()
         file.close()
         assert(len(data) == len(self.energy_fermi))
         ind_ef = 0
         for fila in data:
            vals = map(eval,fila.split())
            assert (len(vals) == number_cases+1)
            for j in range(number_cases):
               dipole[ind_ef][j] += vals[j+1] * dk
               #print >> file1, vals[j] * dk,
               sum_to_dipole = True
            ind_ef +=1

         if(not sum_to_dipole):
            print "WARNING: folder ",name, " is not contributing to Berry curvature dipole integration"     
      print "\n Result of Volume integration [bohr^3]: ", aux_vol, " --This should approximate well the previous output of the volume.\n"
      print "\n Result of Omega integration written in [bohr^(-1)]: "

      file = open("omega_integral_vs_mu.dat",'w')
      for ind_ef in range(len(self.energy_fermi)):
        print >> file, self.energy_fermi[ind_ef], Om_x[ind_ef], Om_y[ind_ef], Om_z[ind_ef]
      file.close()

      file = open("sigma_xy_vs_mu.dat",'w')
      for ind_ef in range(len(self.energy_fermi)):
        print >> file, self.energy_fermi[ind_ef], -Om_z[ind_ef] / self.bohr_to_cm * self.G_0 *np.pi
      file.close()

      file = open("sigma_xz_vs_mu.dat",'w')
      for ind_ef in range(len(self.energy_fermi)):
        print >> file, self.energy_fermi[ind_ef], -Om_y[ind_ef] / self.bohr_to_cm * self.G_0 *np.pi
      file.close()

      file = open("sigma_yz_vs_mu.dat",'w')
      for ind_ef in range(len(self.energy_fermi)):
        print >> file, self.energy_fermi[ind_ef], -Om_x[ind_ef] / self.bohr_to_cm * self.G_0 *np.pi
      file.close()


      if(number_cases > 0):
        print "\n Result of Dipole integrations written in [adim]: \n",
        file = open("dipole_vs_mu.dat",'w')
        for ind_ef in range(len(self.energy_fermi)):
           print >> file, self.energy_fermi[ind_ef], 
           for i in range(number_cases):
              print >> file, dipole[ind_ef][i],
           print >> file,"\n",


  def area_slice(self,**kwargs):

       points = kwargs["points"]
       write_plane = kwargs.pop("write_plane",False)

       #compute geometric center
       r_cm = [0,0,0]
       for p in points:
         for i in range(3):
            r_cm[i] += p[i] / len(points)
       r_c = np.array(r_cm)
       print "Geometric center of the plane is: ", r_cm
       print "Area spanned by G1 and G2: [fplo^2]: ",LA.norm(np.cross(np.array(self.G1),np.array(self.G2)))
       g1p = [self.G1[k]-r_c[k] for k in range(3)]
       g2p = [self.G2[k]-r_c[k] for k in range(3)]
#       print "Area spanned by G1 and G2: [fplo^2]: ",LA.norm(np.cross(np.array(g1p),np.array(g2p)))
       #plane representatives
       points_plane = []
       for p in points:
          p_pi = [p[i]-r_c[i] for i in range(3)]
          points_plane.append(np.array(p_pi))

       #construct basis in the plane
       p_1 = np.array([points_plane[0][i] for i in range(3)])
       p_1_norm = LA.norm(p_1)
       parallel = 1
       k = 1
       while(parallel):
         p_2_aux = np.array([points_plane[k][i] for i in range(3)])
         p_3 = np.cross(p_1,p_2_aux)
         if(LA.norm(p_3)>1e-9):
            parallel = 0
         k+=1
       p_3_norm = LA.norm(p_3)
       p_3 = p_3/p_3_norm
       p_2 = np.cross(p_1,p_3)
       p_2_norm = LA.norm(p_2)
       p_2 = p_2/p_2_norm
       """
       print "p_1:",p_1,np.dot(p_2,p_3)
       print "p_2:",p_2,np.dot(p_1,p_3)
       print "p_3:",p_3,np.dot(p_2,p_1)
       """
       points_plane_2dbasis = []
       for p in points_plane:
          kp = [np.dot(p_1,p),np.dot(p_2,p)]
          assert abs(np.dot(p_3,p)) < 1e-6
          points_plane_2dbasis.append(kp)

       tri = Delaunay(np.array(points_plane_2dbasis))    

       area = 0
       j = 0
       for a in tri.simplices:
#          pA = points_plane_2dbasis[a[0]]
 #         pB = points_plane_2dbasis[a[1]]
  #        pC = points_plane_2dbasis[a[2]]
   #       aux = np.array([[pA[0],pB[0],pC[0]],[pA[1],pB[1],pC[1]],[1,1,1]])
    #      area += 0.5*LA.det(aux)
         pA = points[a[0]]
         pB = points[a[1]]
         pC = points[a[2]]
         AB = np.array([pA[i]-pB[i] for i in range(3)])
         AC = np.array([pA[i]-pC[i] for i in range(3)])
       #  print j,0.5 * LA.norm(np.cross(AB,AC))
         area += 0.5 * LA.norm(np.cross(AB,AC))
         j+=1
       #print "area: ",area

       if(write_plane):
         file = open("plane.dat",'w')
         for p in points:
            print >> file, p[0],p[1],p[2]
         file.close()

         file = open("plane_tri.dat",'w')
         for a in tri.simplices:
          b = [a[0],a[1],a[2],a[0]]
          for p in b:
            for i in range(3):
              print >> file, points[p][i],
            print >> file,"\n",
          print >> file,"\n"
         file.close()
       return area
