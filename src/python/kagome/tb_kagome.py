#!/usr/bin/python


import numpy

t = 1
a = 1.0
sqrt3 = numpy.sqrt(3)

def h_AB(kx,ky):
  return -t*(1+numpy.exp(-1j*kx*a))

def h_AC(kx,ky):
  return -t*(1+numpy.exp(-1j*(kx+sqrt3*ky)*a/2.0))

def h_BC(kx,ky):
  return h_AC(-kx,ky) 


def HK_eig(kx,ky):
   HK = numpy.zeros((3,3), dtype=complex)
   HK[0,1] = h_AB(kx,ky)
   HK[0,2] = h_AC(kx,ky)
   HK[1,2] = h_BC(kx,ky)
   for i in range(3):
     for j in range(3):
        if(i>j):
           HK[i,j] = numpy.conjugate(HK[j,i])

#   print HK
   return numpy.linalg.eig(HK) 

#print HK_eig(0,0)

file = open("dispersion.dat",'w')
K = 2*numpy.pi
klist = [i/100.*K for i in range(100)]

for kx in klist:
  print >> file,kx,
  for eig in HK_eig(kx,0.0)[0]:
    print >> file,eig.real,eig.imag,
  print >> file

file.close()
