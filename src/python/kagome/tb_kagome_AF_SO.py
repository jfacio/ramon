#!/usr/bin/python


import numpy

t = 1
tSO = 0.2 * t      
a = 1.0
theta=numpy.pi/2.0
phi = [0,2.0*numpy.pi/3,4.0*numpy.pi/3]
#phi = [0,0,0]
sqrt3 = numpy.sqrt(3)
J =-1.7*t*2.0
RAB = [-a,0]
RAC = [-a/2,-a*sqrt3/2.]
RBC = [ a/2,-a*sqrt3/2.]

def f(kx,ky,R):
  return 1+numpy.exp(1j*(kx*R[0]+ky*R[1])) 

def U(theta_i,phi_i):
   """Rotation matrix of spin 1/2"""
   Um = numpy.zeros((2,2), dtype=complex)
   Um[0,0] = numpy.cos(theta_i/2.0) * numpy.exp(1j*phi_i/2)
   Um[1,1] = numpy.cos(theta_i/2.0) * numpy.exp(-1j*phi_i/2)
   Um[0,1] = -numpy.sin(theta_i/2.0) * numpy.exp(1j*phi_i/2)
   Um[1,0] = numpy.sin(theta_i/2.0) * numpy.exp(-1j*phi_i/2)
   return numpy.matrix(Um)

def HK(kx,ky): 
   """Kinetic energy in the global basis. Return a matrix <A\sigma|H|B\sigma>"""
   HK = numpy.zeros((6,6), dtype=complex)

   #spin conserving
   HK[0,1] = t * f(kx,ky,RAB)
   HK[0,2] = t * f(kx,ky,RAC)
   HK[1,2] = t * f(kx,ky,RBC)
   HK[3:6,3:6] = HK[0:3,0:3]

   #spin-orbit hoppings
   HK[0,4] = -tSO * f(kx,ky,RAB)
   HK[0,5] = tSO / 2. * (-1+1j*sqrt3) * f(kx,ky,RAC)
   HK[1,4] = tSO  * f(kx,ky,RAB)
   HK[1,5] = tSO / 2. * ( 1+1j*sqrt3) * f(kx,ky,RBC)
   HK[2,4] = tSO / 2. * (-1-1j*sqrt3) * f(kx,ky,RAC)
   HK[2,5] = tSO / 2. * ( 1-1j*sqrt3) * f(kx,ky,RBC)

   #behind the diagonal
   for i in range(6):
     for j in range(6):
        if(i>j):
           HK[i,j] = numpy.conjugate(HK[j,i])

   return numpy.matrix(HK) 

def submatrix(M,al,be):
   S = numpy.zeros((2,2), dtype=complex)
   S[0,0] = M[al,be]
   S[1,1] = M[al+3,be+3]
   S[0,1] = M[al,be+3]
   S[1,0] = M[al+3,be]
   return S

def HK_p_ap_basis(k):
   """Hamiltonian in the parallel-antiparalell basis (where the coupling with the localized spin is diagonal)"""
   kx = k[0]
   ky = k[1]
   #Kinetic energy
   HK_p_ap = numpy.zeros((6,6), dtype=complex)
   HK_global_basis = HK(kx,ky)

   dict_pap_to_global = {}
   dict_pap_to_global[(0,0)]=0 #(site A, parallel) > index 0 in global matrix
   dict_pap_to_global[(0,1)]=3 #(site A, antipara) > index 3 in global matrix
   dict_pap_to_global[(1,0)]=1 #(site B, parallel) > index 1 in global matrix
   dict_pap_to_global[(1,1)]=4 #(site B, antipara) > index 4 in global matrix
   dict_pap_to_global[(2,0)]=2 #(site C, parallel) > index 2 in global matrix
   dict_pap_to_global[(2,1)]=5 #(site C, antipara) > index 5 in global matrix

   for i in range(3): #Aup, Bup, Cup
     for j in range(3): #Aup, Bup, Cup
          HK_ij = submatrix(HK_global_basis,i,j)
          U_i = U(theta,phi[i]).transpose().conjugate()
          U_j = numpy.matrix(U(theta,phi[j]))
#          print i,j,numpy.matrix(HK_ij)
          HK_ij_p_ap = U_i * numpy.matrix(HK_ij) * U_j
#          print i,j,HK_ij_p_ap
          for s in range(2):
            for t in range(2):
 #              print dict_pap_to_global[(i,s)],dict_pap_to_global[(j,t)]
               HK_p_ap[dict_pap_to_global[(i,s)],dict_pap_to_global[(j,t)]] = HK_ij_p_ap[s,t]

   #Coupling to localized spin
   for i in range(3):
       HK_p_ap[i,i] = -J/2.0 #parallel
       HK_p_ap[i+3,i+3] = J/2.0 #antiparallel
 
   return HK_p_ap 

def band_structure(klists,filename):
   file = open(filename,'w')

   kmod=0
   klast = klists[0][0]
   for klist in klists:
     for k in klist:
       print >> file,k[0],k[1],kmod+numpy.sqrt((k[0]-klast[0])**2+(k[1]-klast[1])**2),
       HK_eig = numpy.linalg.eig(HK_p_ap_basis(k))
       for eig in HK_eig[0]:
         print >> file,eig.real,
       print >> file
     kmod+=numpy.sqrt((k[0]-klast[0])**2+(k[1]-klast[1])**2)
     klast = k

   file.close()

"""for calling band_structure"""
K = 4.*numpy.pi/3
Mx =numpy.pi
My =numpy.pi/numpy.sqrt(3)
def KMy(kx):
  return -(kx-K) * My / (K-Mx)

def GMy(kx):
  return kx * My / Mx

klist1 = [[i/100.*K,0] for i in range(100)]
klist2 = [[K-i/100.*(K-Mx),KMy(K-i/100.*(K-Mx))] for i in range(100)]
klist3 = [[Mx-i/100.*(Mx-0),GMy(Mx-i/100.*(Mx))] for i in range(100)]
klists = [klist1,klist2,klist3]
band_structure(klists,'dispersion_AF_SO.dat')
   
def dot_ab(a,b):
   aux = complex(0.0)
   for i in range(len(a)):
      aux += numpy.conjugate(a[i]) * b[i]
   return aux


def smallest_eigenvector(A):
   eigenValues, eigenVectors = numpy.linalg.eig(A)
   idx = eigenValues.argsort()[::-1]   
   eigenValues = eigenValues[idx]
   eigenVectors = eigenVectors[:,idx]
   return eigenVectors[-1],eigenValues[-1]

def berry_curvature(klist):
  delta_al = 0.001
  delta_be = 0.001
  kxold = klist[0][0]
  for k in klist:
       k_loop = [k, [k[0]+delta_al,k[1]], [k[0]+delta_al,k[1]+delta_be], [k[0],k[1]+delta_be]]
       eig_loop = []
       for kp in k_loop:
         vec,val = smallest_eigenvector(HK_p_ap_basis(kp))
         eig_loop.append(vec)
       z =-1.0/(delta_al*delta_be) * numpy.imag(numpy.log(dot_ab(eig_loop[0],eig_loop[1]) * dot_ab(eig_loop[1],eig_loop[2]) * dot_ab(eig_loop[2],eig_loop[3]) * dot_ab(eig_loop[3],eig_loop[0])))
       vec,val = smallest_eigenvector(HK_p_ap_basis(k))
       if((k[0]-kxold)**2 > 1e-10):
         print "\n",
         kxold = k[0]
       print k[0],k[1],z,val

klist = []
for i in range(100):
 for j in range(100):
  klist.append([i*1./100*6,j*1./100*6])
berry_curvature(klist)
