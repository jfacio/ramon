#!/usr/bin/python


import numpy

t = -1
a = 1.0
theta=numpy.pi/2.0
phi = [0,2.0*numpy.pi/3,4.0*numpy.pi/3]
#phi = [0,0,0]
sqrt3 = numpy.sqrt(3)
J = 1.7*t

def h_AB(kx,ky):
  return -t*(1+numpy.exp(-1j*kx*a))

def h_AC(kx,ky):
  return -t*(1+numpy.exp(-1j*(kx+sqrt3*ky)*a/2.0))

def h_BC(kx,ky):
  return h_AC(-kx,ky) 

def U(theta_i,phi_i):
   Um = numpy.zeros((2,2), dtype=complex)
   Um[0,0] = numpy.cos(theta_i/2.0) * numpy.exp(1j*phi_i/2)
   Um[1,1] = numpy.cos(theta_i/2.0) * numpy.exp(-1j*phi_i/2)
   Um[0,1] = -numpy.sin(theta_i/2.0) * numpy.exp(1j*phi_i/2)
   Um[1,0] = numpy.sin(theta_i/2.0) * numpy.exp(-1j*phi_i/2)
   return numpy.matrix(Um)

def HK(kx,ky): 
   """Kinetic energy in the global basis. Return a matrix <A\sigma|H|B\sigma>"""
   HK = numpy.zeros((3,3), dtype=complex)
   HK[0,1] = h_AB(kx,ky)
   HK[0,2] = h_AC(kx,ky)
   HK[1,2] = h_BC(kx,ky)
   for i in range(3):
     for j in range(3):
        if(i>j):
           HK[i,j] = numpy.conjugate(HK[j,i])
   return numpy.matrix(HK) 

def HK_p_ap_basis(kx,ky):
   """Hamiltonian in the parallel-antiparalell basis (where the coupling with the localized spin is diagonal)"""

   #Kinetic energy
   HK_p_ap = numpy.zeros((6,6), dtype=complex)
   HK_global_basis = HK(kx,ky)

   dict_pap_to_global = {}
   dict_pap_to_global[(0,0)]=0 #(site A, parallel) > index 0 in global matrix
   dict_pap_to_global[(0,1)]=3 #(site A, antipara) > index 3 in global matrix
   dict_pap_to_global[(1,0)]=1 #(site B, parallel) > index 1 in global matrix
   dict_pap_to_global[(1,1)]=4 #(site B, antipara) > index 4 in global matrix
   dict_pap_to_global[(2,0)]=2 #(site C, parallel) > index 2 in global matrix
   dict_pap_to_global[(2,1)]=5 #(site C, antipara) > index 5 in global matrix

   for i in range(3): #Aup, Bup, Cup
     for j in range(3): #Aup, Bup, Cup
          HK_ij = numpy.zeros((2,2), dtype=complex)
          HK_ij[0,0] = HK_global_basis[i,j]
          HK_ij[1,1] = HK_global_basis[i,j]
          U_i = U(theta,phi[i]).transpose().conjugate()
          U_j = numpy.matrix(U(theta,phi[j]))
#          print i,j,numpy.matrix(HK_ij)
          HK_ij_p_ap = U_i * numpy.matrix(HK_ij) * U_j
#          print i,j,HK_ij_p_ap
          for s in range(2):
            for t in range(2):
 #              print dict_pap_to_global[(i,s)],dict_pap_to_global[(j,t)]
               HK_p_ap[dict_pap_to_global[(i,s)],dict_pap_to_global[(j,t)]] = HK_ij_p_ap[s,t]

   #Coupling to localized spin
   for i in range(3):
       HK_p_ap[i,i] = -J/2.0 #parallel
       HK_p_ap[i+3,i+3] = J/2.0 #antiparallel
 
   return HK_p_ap 

print HK_p_ap_basis(0,0)
assert 0

file = open("dispersion_AF.dat",'w')
K = 4.*numpy.pi/3
Mx =numpy.pi
My =numpy.pi/numpy.sqrt(3)

def KMy(kx):
  return -(kx-K) * My / (K-Mx)

def GMy(kx):
  return kx * My / Mx

klist1 = [[i/100.*K,0] for i in range(100)]
klist2 = [[K-i/100.*(K-Mx),KMy(K-i/100.*(K-Mx))] for i in range(100)]
klist3 = [[Mx-i/100.*(Mx-0),GMy(Mx-i/100.*(Mx))] for i in range(100)]
kmod=0
klists = [klist1,klist2,klist3]
klast = klists[0][0]
for klist in klists:
  for k in klist:
    print >> file,k[0],k[1],kmod+numpy.sqrt((k[0]-klast[0])**2+(k[1]-klast[1])**2),
    HK_eig = numpy.linalg.eig(HK_p_ap_basis(k[0],k[1]))
    for eig in HK_eig[0]:
      print >> file,eig.real,
    print >> file
    i+=1
  kmod+=numpy.sqrt((k[0]-klast[0])**2+(k[1]-klast[1])**2)
  klast = k

file.close()
