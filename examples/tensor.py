#!/usr/bin/python
import sys,os
sys.path.append(os.path.abspath("/home/jf11/repos/ramon/src/python/"))

import berry_methods

extra_points = False
energy = -0.02
input_curv = "curv_subtrac.dat"
nx = 401
ny = 401
nz = 51
delta_k = 0.00001

kz_names = []
for iz in range(17,34,1):
  kz_names.append("kz_%(iz)s_50"%locals())


B = berry_methods.berry_dipole(nx=nx,ny=ny,nz=nz,root_kz="kz")
#B.split_kz_berry_curvature('curv_subtrac.dat')
#B.substract('../../w90/E_0','curv_subtrac.dat')


#derivo OmegaX respecto de y
B.bd_component(f_index=0,coor_index=1,delta_k=delta_k,kz_names=kz_names,energy_limit = energy, input_curv = input_curv)

#derivo OmegaX respecto de x
B.bd_component(f_index=0,coor_index=0,delta_k=delta_k,kz_names=kz_names,energy_limit = energy, input_curv = input_curv)

#derivo OmegaY respecto de y
B.bd_component(f_index=1,coor_index=1,delta_k=delta_k,kz_names=kz_names,energy_limit = energy, input_curv = input_curv)

#derivo OmegaX respecto de y
B.bd_component(f_index=1,coor_index=0,delta_k=delta_k,kz_names=kz_names,energy_limit = energy, input_curv = input_curv)
