#include <iostream>
#include <fstream>
#include <complex>
#include "WI.hpp"
#include <chrono>

using namespace std::chrono;

int main(int argc, char *argv[])
  {
  double aux;
  std::string base_name;
  if(argc == 3){
    base_name = argv[1];
    aux = std::stod(argv[2]);
    }
  else{
    std::cout << "Error! %s\n" << argv[0];
    return 1;
  }
  TB::WI<std::complex<double> > H(base_name);

  int Nk = 50; //number of points along each point.
//  H.spaghetti(Nk);
//    H.berry_curvature_2dmesh();
  high_resolution_clock::time_point t1 = high_resolution_clock::now();
  H.berry_path(Nk);
  high_resolution_clock::time_point t2 = high_resolution_clock::now();
  auto duration = duration_cast<seconds>( t2 - t1 ).count();
  std::cout << "We did it in: " << duration << "seconds" << std::endl;
  return 0;
  }
