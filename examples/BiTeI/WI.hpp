/*Class for Wanniter interpolation starting from matrix elements H in real space and of positio operator r, as given by wannier90 code. 
    Computes:
    - Bands
    - Berry curvature 
    - Berry dipole

*/
 

#ifndef WI_def 
#define WI_def


#include<map>
#include<armadillo>
#include<vector>
#include<iostream>
#include<sstream>
#include<string>
#include<sys/stat.h>
#include<cassert>
#include <cstddef>

namespace TB{

  bool fileExists(const std::string& filename);
  std::pair<int,std::vector<double> > find_vector(std::string key, std::string line);
  template<class T> std::vector<T> find_str(std::string key, std::string line, int number_of_elements);
  template <class Type> class WI; 
  template<class Type> std::istream& operator>>(std::istream& is, WI<Type> & rhs);


  template <class Type>
  class WI{ 

   public:

      WI(std::string case_name){

         //Check existance of input files
         base_name = case_name;
         std::string file_win = base_name + "_hr.dat";
         std::string file_wout = base_name + ".wout";

         assert((fileExists(file_wout) && fileExists(file_win)) && "Check your input files.");

         //Reading basis vectors from case.wout
         std::ifstream ifs(file_wout);
         std::string line;
         std::pair<int,std::vector<double> > pair;

         while(std::getline(ifs,line)){
                 pair = find_vector("b_1",line);
                 if(pair.first==1) latt_base_K.push_back(pair.second);
                 pair = find_vector("b_2",line);
                 if(pair.first==1) latt_base_K.push_back(pair.second);
                 pair = find_vector("b_3",line);
                 if(pair.first==1) latt_base_K.push_back(pair.second);

                 pair = find_vector("a_1",line);
                 if(pair.first==1) latt_base_R.push_back(pair.second);
                 pair = find_vector("a_2",line);
                 if(pair.first==1) latt_base_R.push_back(pair.second);
                 pair = find_vector("a_3",line);
                 if(pair.first==1) latt_base_R.push_back(pair.second);
         }

         //Reading from Hamiltonian matrix elements from _hr.dat
         std::ifstream ifs2(file_win);
         int i =0;
         int deg_lines; //number of lines used for degeneracies of each point grid.
         int count_deg_points = 0;
         while(std::getline(ifs2,line)){

           if(i==1){
               std::istringstream s(line);
               s >> num_wann;
           }

           if(i==2){
             std::istringstream s(line);
             s >> nrpts;
             deg_lines = nrpts / 15; //15 is a magic number defined by wannier90 code
             //std::cout << "--Lines used for degeneracy information:  " << deg_lines << std::endl;
           }

           if(i>2 && i<=deg_lines+3){
             std::istringstream s(line);
             int aux;
             int k =0;
             while(k<15 && count_deg_points < nrpts){
                s >> aux;
                deg_i.push_back(aux);
                count_deg_points+=1;
                k+=1;
             }
           }

           if(i>deg_lines+3){
             std::istringstream iss(line);
             int ax,ay,az,m,n;
             double Omn_real,Omn_imag;
             if(iss >> ax >> ay >> az >> m >> n >> Omn_real >> Omn_imag) {

                std::complex<double> h(Omn_real,Omn_imag);
                HW_mn[std::make_pair(m-1,n-1)].push_back(h);

                if(m==1 && n==1) 
                  R_coefs_i.push_back(std::vector<double> {1.*ax,1.*ay,1.*az}); 
                //std::cout << ax << ay << az << m << n << Omn_real << Omn_imag << std::endl;
                
             }
             else
               break; 
            
           }

           i++;
         }

         //Some output
         
         std::cout << "Lattice reciprocal vectors: "<< std::endl;
         print_basis(std::vector< std::string > {"b_1= ","b_2= ","b_3= "},latt_base_K);
         std::cout << "Lattice real vectors: "<< std::endl;
         print_basis(std::vector< std::string > {"a_1= ","a_2= ","a_3= "},latt_base_R);
         std::cout << "Lenght of map HW_mn: " << HW_mn.size() << std::endl;
         std::cout << "Lenght of HW_00: " << HW_mn[std::make_pair(0,0)].size() << std::endl;
         for(int i = 0; i < num_wann; i++)
           for(int j = 0; j < num_wann; j++)
               if(HW_mn[std::make_pair(i,j)].size() != R_coefs_i.size())
                     std::cout << "WARNING: Lenght of HW_mn[i,]: " << HW_mn[std::make_pair(i,j)].size() << std::endl;
         std::cout << "Lenght of rhs: " << R_coefs_i.size() << std::endl;
         std::cout << "Lenght of deg_i: " << deg_i.size() << std::endl;
         
      }; 

      void spaghetti(int Nk){
         /*
         Computes bands structure from klist in .win
         */
         read_kpath(Nk);

         std::string file_output = base_name + ".spag";
         std::ofstream ofs(file_output);
         int p = 0;
         for(auto it = klist_points.begin(); it != klist_points.end(); ++it){
             arma::cx_mat HK(num_wann,num_wann);
             HK = build_HK(*it,"K");
             arma::cx_mat eig_vec(num_wann,num_wann);
             arma::vec eig_vals;
             arma::eig_sym(eig_vals, eig_vec, HK);
             ofs << relative_position[p] << " ";
             for(int i =0; i < num_wann; i++)
                   ofs << eig_vals[i] << " ";
             
             ofs << std::endl;
             p += 1;
         }
         
      }

      void berry_path(int Nk){
         /*
           Computes berry curvature for kpath in .win
           Input: Nk, number of k-points in each high symmetry path contained in the full path.
         */
         read_kpath(Nk);
         std::cout << "Reading position operator matrix elements" << std::endl;
         read_position_operator();
         std::cout << "Lenght of map rW_mn: " << rW_mn.size() << std::endl;
         std::cout << "Lenght of rW_00: " << rW_mn[std::make_pair(0,0)].size() << std::endl;

         std::string file_output = base_name + ".berry";
         std::ofstream ofs(file_output);
         std::vector<double> K, om_total;


         int p =0;
         for(auto it = klist_points.begin(); it != klist_points.end(); ++it){
             K = get_vector(*it,"K");
             ofs << relative_position[p] << " " ;
             Omega(K,om_total);
             for(int i = 0; i < 3; i++)
                 ofs << om_total[i] << " " ;

             ofs << std::endl;
             p+=1;
         }
         
      }

      void berry_curvature_2dmesh(){//TO BE DISGUISED
         std::string file_win = base_name + ".win",line;
         std::ifstream ifs(file_win);
         std::vector<double> kslice_corner, kslice_b1, kslice_b2, aux;
         std::vector<int> nk,aux2;
         while(std::getline(ifs,line)){
             aux = find_str<double>("kslice_corner",line,3);
             if(aux.size() ==3)
               kslice_corner = aux;
             aux = find_str<double>("kslice_b1",line,3);
             if(aux.size() ==3)
               kslice_b1 = aux;
             aux = find_str<double>("kslice_b2",line,3);
             if(aux.size()==3)
               kslice_b2 = aux;
             aux2 = find_str<int>("kslice_2dkmesh",line,2);
             if(aux2.size()==2)
               nk = aux2;
        }


        for(int i = 0; i < nk[0] +1; i++)
          for(int j = 0; j < nk[1] +1; j++){
            double nk0 = kslice_corner[0] + i*1.*kslice_b1[0]/nk[0] + j*1.*kslice_b2[0]/nk[1];
            double nk1 = kslice_corner[1] + i*1.*kslice_b1[1]/nk[0] + j*1.*kslice_b2[1]/nk[1];
            double nk2 = kslice_corner[2] + i*1.*kslice_b1[2]/nk[0] + j*1.*kslice_b2[2]/nk[1];
            klist_points.push_back(get_vector(std::vector<double>{nk0,nk1,nk2},"K"));
          }
/*
         std::cout << "Number of kpoints in slice: " << klist_points.size() << std::endl;
         std::string file_output = base_name + ".berry_total";
         std::ofstream ofs(file_output);
         for(auto it = klist_points.begin(); it != klist_points.end(); ++it){
             //double om_z = Omega_al_be(*it,0,1,1e-5);
             double om_z = Omega_al_be_kubo(*it,0,1);
             ofs << (*it)[0] << " " << (*it)[1] << " " << (*it)[2] << " " << om_z << std::endl;
         }
*/

      }

   private:
      std::map <std::pair<int, int>, std::vector<Type> > HW_mn; 
      //map between indices n,m (corresponding to Wannier orbitals) and vector of matrix elements. Each j vector componenct correspond to the matrix element between the site 0 and site j and Wannier orbitals n and m.

      std::map <std::pair<int, int>, std::vector<std::vector<Type> > > rW_mn;
      //idem as HW_mn, but has three elements: r_x, r_y and r_z

      std::vector<std::vector<double> > R_coefs_i; 
      //vector of coeficients of the R vectors for which matrix elements are computede, written in the A_i basis. The i-esim component is (a_1i, a_2i, a_3i) where R_i = a_1i * \vec{a1} + a_2i * \vec{a2} + a_3i * \vec{a3}

      std::vector<int> deg_i;
      //weight of i-point is 1/deg_i[i] (convention from Wannier90)

      std::string base_name; //name root for the w90 files
      int nrpts; //number of Wigner-Seitz grid point
      int num_wann; //number of  Wannier orbitals in each site
      std::vector<std::vector<double> > klist_points; //list of points used for different purposes. 
      std::vector<double> relative_position; //when using kpath methods, here we store the position of each k-point along the path
      std::vector<std::vector<double> > latt_base_R; //Real space basis vectors in cartesian coordinates
      std::vector<std::vector<double> > latt_base_K; //Momentum space basis vectors in cartesian coordinates

      //FOURIER TRANSFORM METHODS      
      arma::cx_mat build_HK(const std::vector<double> &kp, std::string base_kp){
         /*
           Builds H(k) = 1/N \sum_R e^{ikR}Hnm(R)
  
           Input:
           -kp: vecotr of coordinates in basis specified by the string base_kp
           -base_kp: "K" for "cartesian"

           Output: H(k) matrix 
         */

         const std::complex<double> icomplex(0, 1);
         arma::cx_mat HK(num_wann,num_wann);
         HK.zeros();
         std::vector<double> K,R;

         if(base_kp=="K")
            K = get_vector(kp,"K");
         else{
            assert(base_kp=="cartesian" && "Check the basis of your k-point");
            K = kp;
         }

         for(int m = 0; m < num_wann ; m++) 
           for(int n = 0; n < num_wann ; n++){
               auto key = std::make_pair(m,n);
               std::complex<double> aux(0.0,0.0);
               for(int i=0; i<  HW_mn[key].size(); i++){
                    R = get_vector(R_coefs_i[i],"R");
                    aux += std::exp(icomplex*dot(R,K)) * HW_mn[key][i]/(1.0*deg_i[i]);
               }
               HK(m,n) = aux;
           }

         return HK;
      }

      void build_all_for_curv(const std::vector<double> &kp, arma::cx_mat &U_rot, std::vector<arma::cx_mat> &V,std::vector<arma::cx_mat> &A, std::vector<std::vector<arma::cx_mat> >&Om_bar){
         /*
           Builds V_al(k) = 1/N \sum_R i R_al e^{ikR}Hnm(R)
                  A_al(k) = 1/N \sum_R e^{ikR}rnm(R)
                  Om_bar_al_be(k) = 1/N \sum_R e^{ikR} (R_al rnm(R)_be - R_be rnm(R)_al) i
  
           Input:
           -kp: vector in cartesian coordinates
           -U: rotation matrix connecting Hamiltonian and Wannier gauges
         */

         const std::complex<double> icomplex(0, 1);
         std::vector<double> R;

         for(int al = 0; al < 3; al++){
            V[al].zeros();
            A[al].zeros();
            for(int be = 0; be < 3; be++)
                Om_bar[al][be].zeros();
         }

         for(int m = 0; m < num_wann ; m++) 
           for(int n = 0; n < num_wann ; n++){
               auto key = std::make_pair(m,n);

               for(int i=0; i<  HW_mn[key].size(); i++){
                    R = get_vector(R_coefs_i[i],"R");
                    for(int al = 0; al < 3; al++){
                        V[al](m,n) += icomplex * R[al] * std::exp(icomplex*dot(R,kp)) * HW_mn[key][i] / (1.0*deg_i[i]);
                        A[al](m,n) += std::exp(icomplex*dot(R,kp)) * rW_mn[key][i][al] / (1.0*deg_i[i]);
                        for(int be = 0; be < 3; be++)
                           Om_bar[al][be](m,n) += icomplex * std::exp(icomplex*dot(R,kp)) * (R[al] * rW_mn[key][i][be] - R[be] * rW_mn[key][i][al]) / (1.0*deg_i[i]);
                    }
               }
           }

         for(int al = 0; al < 3; al++){
            V[al] = U_rot.t() * V[al] * U_rot;
            A[al] = U_rot.t() * A[al] * U_rot;
            for(int be = 0; be < 3; be++)
               Om_bar[al][be] = U_rot.t() * Om_bar[al][be] * U_rot;
         }

      }


      /*K path - slices readers*/
      void read_kpath(const int &Nk){
         /*
          Reads k-pathes from case.win and fills (*this).klist_points and (*this).relative_positions
         */
         std::string file_win = base_name + ".win", line, nc,nd;
         std::string::size_type n,m;
         double c1,c2,c3;
         double d1,d2,d3;

         std::ifstream ifs(file_win);
         std::vector<std::pair<std::vector<double>,std::vector<double> > > kpathes;
         while(std::getline(ifs,line)){
            n=line.find("begin kpoint_path");
            if(n != std::string::npos){
               do{
                 std::getline(ifs,line);
                 std::istringstream s(line);
                 s >> nc >>  c1 >> c2 >> c3 >> nd >>  d1 >> d2 >> d3;
                 std::vector<double> k_i = std::vector<double> {c1,c2,c3};
                 std::vector<double> k_f = std::vector<double> {d1,d2,d3};
                 kpathes.push_back(std::make_pair(k_i,k_f));
                 m = line.find("end kpoint_path");
               }
               while(m == std::string::npos);
            }
         }
         kpathes.pop_back(); 
         std::cout << "Number of paths: " << kpathes.size() << std::endl;

         std::vector<double> kp={0.0,0.0,0.0},k_i,k_f, diff={0.0,0.0,0.0};

         double coor, coor_prev = 0;
         for(auto it = kpathes.begin(); it != kpathes.end(); ++it){
            k_i = (*it).first;
            k_f = (*it).second;
            for(int i=0; i<3; i++)
                diff[i] = k_f[i]-k_i[i];
            diff = get_vector(diff,"K");
            double length_path = std::sqrt(diff[0]*diff[0] + diff[1]*diff[1]+diff[2]*diff[2]);
            for(int k=0;k<=Nk;k++){
               for(int i=0; i<3; i++)
                 kp[i] = ((Nk-k)*k_i[i] + k*k_f[i])/Nk;

               klist_points.push_back(kp);
               //and its relative coordinate along the path, for the plot
               for(int i=0; i<3; i++)
                 diff[i] = kp[i]-k_i[i];
               diff = get_vector(diff,"K");
               coor = coor_prev + std::sqrt(diff[0]*diff[0] + diff[1]*diff[1]+diff[2]*diff[2]) ;
               relative_position.push_back(coor);
               
            }
            coor_prev += length_path;
         }
         std::cout << "Number of kpoints in path: " << klist_points.size() << std::endl;
      }

      //make 2D and 3D analogues


      // BERRY CURVATURE METHODS

      void Omega(const std::vector<double> &kp, std::vector<double> &ans){
          /*
          Compute total Berry curvature 'á la Wannier'
          */
          ans = {0.,0.,0.};

          //diagonalize HK
          arma::cx_mat HK_kp(num_wann,num_wann), U(num_wann,num_wann);
          arma::vec eig_vals;
          HK_kp = build_HK(kp,"cartesian");
          arma::eig_sym(eig_vals,U,HK_kp);

          //Build auxiliary operators Eq. 38-40 in WYSV
          std::vector<arma::cx_mat> V,A;
          std::vector<std::vector<arma::cx_mat > > Omega_bar; 
          for(int i = 0; i < 3; i++){
            V.push_back(U);
            A.push_back(U);
            Omega_bar.push_back({U,U,U});
          }
          build_all_for_curv(kp,U,V,A,Omega_bar);
          std::vector<arma::cx_mat> D = build_D(V,eig_vals); //DH operator
 
          for(int coor = 0; coor < 3; coor++){
             int al,be;
             if(coor==0) al=1,be=2;
             if(coor==1) al=2,be=0;
             if(coor==2) al=0,be=1;

             //Eq. 32 in WYSV
             std::complex<double> aux_ab, aux_ba;
             for(int n = 0; n < num_wann ; n++)
               if(eig_vals[n] < 0){

                 //Om_H_bar contribution
                 ans[coor] -= 0.5*std::real(Omega_bar[al][be](n,n)-Omega_bar[be][al](n,n));

                 for(int m = 0; m < num_wann ; m++)
                   if(eig_vals[m] >0){
                     //DD
                     aux_ab = V[al](n,m)*V[be](m,n) / std::pow(eig_vals[n]-eig_vals[m],2);
                     aux_ba = V[be](n,m)*V[al](m,n) / std::pow(eig_vals[n]-eig_vals[m],2);
                     ans[coor] += std::imag(aux_ab-aux_ba);
                     //DA
                     aux_ab = D[al](n,m)*A[be](m,n) -  D[be](n,m)*A[al](m,n);
                     aux_ba = D[be](n,m)*A[al](m,n) -  D[al](n,m)*A[be](m,n);
                     ans[coor] -= std::real(aux_ab-aux_ba);
                   }
               }

          }
      }

      std::vector<arma::cx_mat> build_D(const std::vector<arma::cx_mat> &V, const arma::vec &eig_vals){
         /*Eq. 24 in WYSV
         Builds DH_al matrix: D^H_\al(k) = V^H_al(k) / (e_m-e_n) if n!=m (0 otherwise)
         
         Input:
         - V^H(al)
         - eig_vals

         Output: [DH_x, DH_y, DH_z] matrix
         */
         arma::cx_mat DH_x(num_wann,num_wann), DH_y(num_wann,num_wann), DH_z(num_wann,num_wann);
         DH_x.zeros();
         DH_y.zeros();
         DH_z.zeros();

         for(int m = 0; m < num_wann ; m++) 
           for(int n = 0; n < num_wann ; n++)
              //if(n!=m && std::abs(eig_vals[m]-eig_vals[n])>0.0001){
              if(n!=m && eig_vals[m]<0 && eig_vals[n]>0){
               DH_x(m,n) = V[0](m,n) / (eig_vals[m]-eig_vals[n]);
               DH_y(m,n) = V[1](m,n) / (eig_vals[m]-eig_vals[n]);
               DH_z(m,n) = V[2](m,n) / (eig_vals[m]-eig_vals[n]);
              }

         return std::vector<arma::cx_mat> {DH_x,DH_y,DH_z}; 

      }

      void read_position_operator(void){
         //Read position operator from case_r.dat

         std::string file_wr = base_name + "_r.dat"; 
         std::string line;
         std::ifstream ifs2(file_wr);
         int ax,ay,az,m,n;
         double rx_real,rx_imag, ry_real,ry_imag, rz_real,rz_imag;
         int i =0;
         while(std::getline(ifs2,line)){
            if(i>1){
                 std::istringstream iss(line);
                 if(iss >> ax >> ay >> az >> m >> n >> rx_real >> rx_imag >> ry_real >> ry_imag >> rz_real >> rz_imag){
                     std::complex<double> rx(rx_real,rx_imag), ry(ry_real,ry_imag), rz(rz_real,rz_imag);
                     rW_mn[std::make_pair(m-1,n-1)].push_back(std::vector<std::complex<double> > {rx,ry,rz});
                 }
                 else 
                   break;
            }

            i++;
         }

      }

      //AUXILIARY FUNCTIONS
      void print_basis(const std::vector< std::string > &bs_names, const std::vector<std::vector<double> > &base){
         for(int i=0;i<3;i++)
           for(int j=0;j<3;j++){
            if(j==0)
               std::cout << std::endl << bs_names[i] << base[i][j] << " ";
            else
               std::cout << base[i][j]  << " ";
         }
         std::cout << std::endl << std::endl;
      }

      double dot(const std::vector<double> & v1, const std::vector<double> & v2){
         double ans;
         for(int k = 0; k< 3; k++)
            ans += v1[k] * v2[k];
         return ans;
      }

      std::vector<double> get_vector(const std::vector<double> &coefs, const char* B){
         std::vector<double> ans({0.0,0.0,0.0});
         std::vector<std::vector<double> > base = B=="R" ? latt_base_R : latt_base_K;

         for(int i=0; i<3; i++)
            for(int j=0; j<3; j++)
                 ans[i] += coefs[j] * base[j][i];
         return ans;
      }


   };


  bool fileExists(const std::string& filename){
    struct stat buf;
    if(stat(filename.c_str(),&buf)!=-1)
      return true;
    return false;
  }

  std::pair<int,std::vector<double> > find_vector(std::string key, std::string line){
     std::string aux;
     std::string::size_type n;
     double c1,c2,c3;

     n=line.find(key);

     if(n != std::string::npos){
       std::istringstream s(line);
       s >> aux >> c1 >> c2 >> c3;
       return std::make_pair(1,std::vector<double> {c1,c2,c3});
     }
     else return std::make_pair(0,std::vector<double> {0.0,0.0,0.0});                
  }

  template<class T>
  std::vector<T> find_str(std::string key, std::string line, int number_of_elements){
     std::vector<T> ans;
     std::string aux;
     std::string::size_type n;
     T c;

     n=line.find(key);

     if(n != std::string::npos){
       std::istringstream s(line);
       s >> aux >> aux;
       for(int i = 0; i < number_of_elements; i++){
           s >> c;
          ans.push_back(c);
       }
     }
     return ans;
  }

}
#endif
